import java.util.Scanner;

class FahrkartenautomatAusgelagert
{	
    public static void main(String[] args)
    {

      
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double einzelpreis;
       double r�ckgabebetrag;
       short anzahltickets;


       

       // Geldeinwurf
       // -----------
       
       geldeinwurf();

       // Fahrscheinausgabe
       // -----------------

       fahrscheinausgabe();

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

    	   r�ckgabebetrag =  RueckgabebetragBerechnen(r�ckgabebetrag, 2.0);
    	   r�ckgabebetrag =  RueckgabebetragBerechnen(r�ckgabebetrag, 1.0);
    	   r�ckgabebetrag =  RueckgabebetragBerechnen(r�ckgabebetrag, 0.5);
    	   r�ckgabebetrag =  RueckgabebetragBerechnen(r�ckgabebetrag, 0.2);
    	   r�ckgabebetrag =  RueckgabebetragBerechnen(r�ckgabebetrag, 0.1);
    	   r�ckgabebetrag =  RueckgabebetragBerechnen(r�ckgabebetrag, 0.05);
    	   
    	   
    	   
           
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    
    public static void geldeinwurf(double zuZahlenderBetrag, double eingezahlterGesamtbetrag, double eingeworfeneM�nze, double einzelpreis, short anzahltickets)
    {
        Scanner tastatur = new Scanner(System.in);
        

        
    	System.out.println("wie viel Kostet ein Ticket?");
        einzelpreis = tastatur.nextDouble();
       
        System.out.println("Wie viele Tickets m�chten sie Kaufen?");
        anzahltickets = tastatur.nextShort();
        zuZahlenderBetrag = einzelpreis * anzahltickets;
        
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.print("Noch zu zahlen: ");
     	   System.out.printf("%.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag);
     	   System.out.println(" Euro");
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
           return;
        }
    }
    
    
    /// f�hrt Phase Fahrtscheinausgabe aus
    /// �bergabeparameter: keine 
    /// R�ckgabewert: keiner
    public static void fahrscheinausgabe()							    
    {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try 
           {
        	   Thread.sleep(250);
           } 
           catch (InterruptedException e) 
           {
        	   e.printStackTrace();
           }
        }
        System.out.println("\n\n");
        return;
    }
    
    
    
    public static double RueckgabebetragBerechnen(double rueckgabebetrag, double vergleichswert) 
    {
    	while(rueckgabebetrag >= vergleichswert) 
        {
    		System.out.println("%.2f �", vergleichswert);
	        rueckgabebetrag -= vergleichswert;
        }
		return rueckgabebetrag;
	}
}

